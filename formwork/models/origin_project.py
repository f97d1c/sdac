from django.db import models
from .origin_dynamic_year_base_model import OriginDynamicYearBaseModel


class OriginProject(OriginDynamicYearBaseModel):
    id                        = models.UUIDField(primary_key=True, editable=False)
    project_code              = models.CharField(max_length=64, verbose_name='项目编号')
    project_name              = models.CharField(max_length=160, verbose_name='项目名称')
    kind                      = models.CharField(max_length=255, null=True, blank=True, default=None, verbose_name='组织形式代码')
    kind_name                 = models.CharField(max_length=255, null=True, blank=True, default=None, verbose_name='组织形式名称')
    pur_method                = models.CharField(max_length=20, null=True, blank=True, default=None, verbose_name='采购方式')
    project_type              = models.CharField(max_length=20, null=True, blank=True, default=None, verbose_name='项目类别')
    region                    = models.CharField(max_length=255, verbose_name='项目所属地区')
    EXT1                      = models.CharField(max_length=100, null=True, blank=True, default=None, verbose_name='项目概况')
    EXT2                      = models.CharField(max_length=100, null=True, blank=True, default=None, verbose_name='采购需求')
    budget_money              = models.DecimalField(null=True, blank=True, default=None, max_digits=14, decimal_places=4, verbose_name='预算金额')
    catalogue_code            = models.CharField(max_length=20, null=True, blank=True, default=None, verbose_name='品目编号')
    catalogue_name            = models.CharField(max_length=30, null=True, blank=True, default=None, verbose_name='品目名称')
    other_supplement          = models.TextField(null=True, blank=True, verbose_name='其他补充事项')
    implement_policy          = models.TextField(null=True, blank=True, verbose_name='采购项目需落实的政府采购政策')
    manager_name              = models.CharField(max_length=80, null=True, blank=True, default=None, verbose_name='项目联系人')
    manager_link_phone        = models.CharField(max_length=255, null=True, blank=True, default=None, verbose_name='项目联系电话')
    project_create_time       = models.CharField(max_length=20, null=True, blank=True, default=None, verbose_name='项目创建时间')
    plan_id                   = models.CharField(max_length=64, null=True, blank=True, default=None, verbose_name='计划ID')
    plan_codes                = models.CharField(max_length=50, null=True, blank=True, default=None, verbose_name='计划编号')
    plan_name                 = models.CharField(max_length=1024, null=True, blank=True, default=None, verbose_name='计划名称')
    plan_moneys               = models.DecimalField(null=True, blank=True, default=None, max_digits=14, decimal_places=4, verbose_name='计划金额')
    purchaser_code            = models.CharField(max_length=50, null=True, blank=True, default=None, verbose_name='单位编码')
    purchaser                 = models.CharField(max_length=80, null=True, blank=True, default=None, verbose_name='采购单位名称')
    purchaser_address         = models.CharField(max_length=255, null=True, blank=True, default=None, verbose_name='采购单位地址')
    purchaser_link_name       = models.CharField(max_length=126, null=True, blank=True, default=None, verbose_name='采购单位开户行')
    purchaser_link_tel        = models.CharField(max_length=50, null=True, blank=True, default=None, verbose_name='采购人联系电话')
    agency_bank               = models.CharField(max_length=20, null=True, blank=True, default=None, verbose_name='')
    agency_account            = models.CharField(max_length=30, null=True, blank=True, default=None, verbose_name='采购单位账号')
    agent_name                = models.CharField(max_length=60, null=True, blank=True, default=None, verbose_name='代理机构名称')
    agent_address             = models.CharField(max_length=100, null=True, blank=True, default=None, verbose_name='代理机构地址')
    agent_link_name           = models.CharField(max_length=50, null=True, blank=True, default=None, verbose_name='代理机构联系人')
    agent_link_tel            = models.CharField(max_length=255, null=True, blank=True, default=None, verbose_name='代理机构联系电话')
    agent_sociCer_num         = models.CharField(max_length=60, null=True, blank=True, default=None, verbose_name='代理机构统一信用代码')
    agent_link_email          = models.CharField(max_length=30, null=True, blank=True, default=None, verbose_name='代理机构邮箱')
    PPP                       = models.CharField(max_length=20, null=True, blank=True, default=None, verbose_name='PPP项目标识')
    contract_end_date         = models.DateTimeField(null=True, blank=True, default=None, verbose_name='合同履约期限')
    create_time               = models.DateTimeField(verbose_name='创建时间')
    update_time               = models.DateTimeField(null=True, blank=True, default=None, verbose_name='')
    tenant_id                 = models.CharField(max_length=60, null=True, blank=True, default=None, verbose_name='租户id')
    region_code               = models.CharField(max_length=20, null=True, blank=True, default=None, verbose_name='区划编码')
   

    class Meta:
        db_table              = 'project'
        verbose_name          = '项目表'
        verbose_name_plural   = '项目表'
        managed               = False