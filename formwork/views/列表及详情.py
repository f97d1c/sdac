from django.core.paginator import Paginator
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.urls import reverse, reverse_lazy
from django.contrib import messages
# from centre.views.share_import import *

from django.views.generic import ListView, DetailView
from django.views.generic.edit import FormView, CreateView, UpdateView, DeleteView

from django.contrib.messages.views import SuccessMessageMixin

from centre.models import 驼峰类名

import re

class 驼峰类名ListView(ListView):
    model = 驼峰类名
    context_object_name = '蛇形类名复数'
    paginate_by = 10
    template_name = 'centre/蛇形类名复数/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        query_dict = self.request.GET.dict()
        context['query'] = query_dict
        return context

    def get_queryset(self):
        try:
            dict = self.request.GET.dict()
            fields = []
            for field in self.model._meta.get_fields():
                fields.append(field.name)

            reg_list = list(map(lambda x: x + "_*", fields))
            reg_str = "|".join(reg_list)

            for key, value in dict.copy().items():
                if (not re.compile(reg_str).match(key)) or (value == ""):
                    dict.pop(key)
                    continue
                
            per_page = self.request.COOKIES.get('per_page')
            self.paginate_by = per_page if int(per_page) else self.paginate_by
            return self.model.objects.order_by("排序字段").filter(**dict)
        except self.model.DoesNotExist:
            return self.model.objects.none()

class 驼峰类名DetailView(DetailView):
    model = 驼峰类名
    template_name = 'centre/蛇形类名复数/show.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["verbose_object"] = self.object.get_display()
        return context