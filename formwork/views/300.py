

class 驼峰类名CreateView(SuccessMessageMixin, CreateView):
    model = 驼峰类名
    fields = "__all__"
    template_name = '子项目名称/蛇形类名复数/form.html'
    success_url = reverse_lazy('子项目名称_蛇形类名复数_index')
    success_message = '创建成功！'


class 驼峰类名UpdateView(SuccessMessageMixin, UpdateView):
    model = 驼峰类名
    fields = "__all__"
    template_name = '子项目名称/蛇形类名复数/form.html'
    success_url = reverse_lazy('子项目名称_蛇形类名复数_index')
    success_message = '修改成功！'


class 驼峰类名DeleteView(DeleteView):
    model = 驼峰类名
    success_url = reverse_lazy('子项目名称_蛇形类名复数_index')

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, '删除成功！')
        return super(驼峰类名DeleteView, self).delete(request, *args, **kwargs)

