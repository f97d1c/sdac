#!/bin/bash --login

echo -e "$sub_project_name/urls.py 文件手动添加:\n"

echo "from $sub_project_name.views.$class_names import *"

echo "
    # $manage_name
    path('$class_names/index',        ${className}ListView.as_view(),   name='${sub_project_name}_${class_names}_index'),
    path('$class_names/<pk>/show',    ${className}DetailView.as_view(), name='${sub_project_name}_${class_name}_show'),"
if [ "$need_new_edit_delete" == 'true' ]; then
  echo "    path('$class_names/create',       ${className}CreateView.as_view(), name='$templates_create_url'),
    path('$class_names/<pk>/update',  ${className}UpdateView.as_view(), name='$templates_update_url'),
    path('$class_names/<pk>/delete',  ${className}DeleteView.as_view(), name='$templates_delete_url'),
"
fi