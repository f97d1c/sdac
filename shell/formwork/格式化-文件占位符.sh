#!/bin/bash --login
echo "开始格式化文件占位符"
cat "$1" | sed "s/驼峰类名/$className/g" | sed "s/蛇形类名复数间隔/$class_names_split/g" | sed "s/蛇形类名复数/$class_names/g" | sed "s/蛇形类名/$class_name/g" | sed "s/管理名称/$manage_name/g" | sed "s/子项目名称/$sub_project_name/g" | sed "s/详情名称/$model_display_name/g" >"$1"

if [ ! -s "$1" ];then
  echo "文件为空: $1"
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
fi