#!/bin/bash --login
echo "开始初始化参数"

# params=$(cat "$main_path/config/pages/公告项目.json")
# params=$(cat "$main_path/config/pages/公告正文.json")
# params=$(cat "$main_path/config/pages/中标供应商.json")
# params=$(cat "$main_path/config/pages/中标公告.json")
# params=$(cat "$main_path/config/pages/中标合同公告.json")
# params=$(cat "$main_path/config/pages/采购公告.json")

if [ ! -s "$1" ];then
  echo "配置文件内容为空: $1"
  exit 255
fi

params=$(cat "$1")
jq '.' <<<$params
project_path=$(jq -r '.["项目路径"]' <<<$params)
sub_project_name=$(jq -r '.["子项目名称"]' <<<$params)
sub_project_path="$project_path/$sub_project_name"
className=$(jq -r '.["驼峰类名"]' <<<$params)
class_name=$(jq -r '.["蛇形类名"]' <<<$params)
class_names=$(jq -r '.["蛇形类名复数"]' <<<$params)
class_names_split=$(sed "s/_/ /g" <<<$class_names)

model_display_name=$(jq -r '.["模型展示名称"]' <<<$params)
manage_name="$model_display_name管理"
page_show_relation_keys=($(jq -r '.["页面参数"]["详情"]["关联显示"]//[]' <<<$params | jq -r 'keys_unsorted[]'))
page_show_relation_values=($(jq -r '.["页面参数"]["详情"]["关联显示"]//{}' <<<$params | jq -r '.[]'))


# 增删改查相关参数
templates_form_path="$project_path/templates/$sub_project_name/$class_names/form.html"
need_new_edit_delete=$(jq -r '.["页面参数"]["增删改"]//false' <<<$params)
templates_create_url="${sub_project_name}_${class_names}_create"

templates_update_url="${sub_project_name}_${class_name}_update"
templates_delete_url="${sub_project_name}_${class_name}_delete"