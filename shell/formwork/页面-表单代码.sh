#!/bin/bash --login

rm -f $templates_form_path

if [ "$need_new_edit_delete" != 'true' ]; then 
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 0; else exit 0; fi
fi

echo "生成 templates/form 代码"

mkdir -p "${templates_form_path%/*}" && touch $templates_form_path
cat "$main_path/formwork/templates/form/form.html" >>$templates_form_path

source "$main_path/shell/formwork/格式化-文件占位符.sh" "$templates_form_path"