#!/bin/bash --login

echo "写入模型初始化文件"
model_init_file="$sub_project_path/models/__init__.py"
cat $model_init_file | grep $className 1>/dev/null
if [[ $? -eq 0 ]]; then
  echo "$model_init_file 已存在 $className 相关定义, 停止写入."
else
  echo "from .$class_name import $className" >>$model_init_file
fi