#!/bin/bash --login

echo "生成 view 代码"
view_path="$sub_project_path/views/$class_names.py"
order_field=$(jq -r '.["页面参数"]["列表"]["排序字段"]' <<<$params)

if [ $order_field == 'null' ]; then
  order_field='-id'
fi

rm -f $view_path && touch $view_path

cat "$main_path/formwork/views/100.py" >>$view_path

for value in "${page_show_relation_values[@]}"; do
  echo "        context[\"verbose_$value\"] = self.object.$value.get_display()" >>$view_path
done

cat "$main_path/formwork/views/200.txt" >>$view_path

if [ "$need_new_edit_delete" == 'true' ]; then
  cat "$main_path/formwork/views/300.py" >>$view_path
fi

cat "$view_path" | sed "s/子项目名称/$sub_project_name/g" | sed "s/驼峰类名/$className/g" | sed "s/蛇形类名复数/$class_names/g" | sed "s/排序字段/$order_field/g" >$view_path

if [ ! -s "$view_path" ];then
  echo "文件为空: $view_path"
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
else
  cat $view_path
fi