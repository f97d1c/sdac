#!/bin/bash --login

echo "生成 templates/show 代码"
templates_show_path="$project_path/templates/$sub_project_name/$class_names/show.html"
rm -f $templates_show_path
mkdir -p "${templates_show_path%/*}" && touch $templates_show_path

cat "$main_path/formwork/templates/show/100.html" >>$templates_show_path

cat "$main_path/formwork/templates/show/detail.html" | sed "s/键值变量名/verbose_object/g" >>$templates_show_path

for index in "${!page_show_relation_keys[@]}"; do
  key=${page_show_relation_keys[index]}
  value=${page_show_relation_values[index]}
  cat "$main_path/formwork/templates/show/detail.html" | sed "s/详情名称/$key/g" | sed "s/键值变量名/verbose_$value/g" >>$templates_show_path
done

cat "$main_path/formwork/templates/show/200.html" >>$templates_show_path

cat $templates_show_path | sed "s/驼峰类名/$className/g" | sed "s/蛇形类名复数间隔/$class_names_split/g" | sed "s/蛇形类名复数/$class_names/g" | sed "s/蛇形类名/$class_name/g" | sed "s/管理名称/$manage_name/g" | sed "s/子项目名称/$sub_project_name/g" | sed "s/详情名称/$model_display_name/g" >$templates_show_path

if [ ! -s "$templates_show_path" ];then
  echo "文件为空: $templates_show_path"
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
else
  cat $templates_show_path
fi