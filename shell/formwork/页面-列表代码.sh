#!/bin/bash --login

echo "生成 templates/index 代码"
templates_index_path="$project_path/templates/$sub_project_name/$class_names/index.html"

# if [ ! -d "${templates_index_path%/*}" ]; then
#   mkdir -p "${templates_index_path%/*}"
# elif [ -f "$templates_index_path" ]; then
#   echo "删除已存在文件: $templates_index_path"
#   rm -f "$templates_index_path"
# fi
# echo "创建文件: $templates_index_path"
# touch "$templates_index_path"

rm -f $templates_index_path
mkdir -p "${templates_index_path%/*}" && touch $templates_index_path

list_keys=($(jq -r '.["页面参数"]["列表"]["显示字段"]' <<<$params | jq -r 'keys_unsorted[]'))
list_values=($(jq -r '.["页面参数"]["列表"]["显示字段"][]' <<<$params))

search_keys=($(jq '.["页面参数"]["列表"]["搜索字段"]' <<<$params | jq -r 'keys_unsorted[]'))

cat "$main_path/formwork/templates/index/100.html" >>$templates_index_path

echo '写入搜索条件'
for index in "${!search_keys[@]}"; do
  key=${search_keys[index]}
  search_value="$(jq -r --arg key $key '.["页面参数"]["列表"]["搜索字段"]["\($key)"]' <<<$params)"

  case "$search_value" in
  'null')
    value=$(jq -r --arg key $key '.["页面参数"]["列表"]["显示字段"]["\($key)"]' <<<$params)
    echo "                $key: <input id=\"${value}__exact\" class=\"form-control input-sm inline w200\" max-length=\"20\" name=\"${value}__exact\" type=\"text\" value=\"{{ request.GET.${value}__exact }}\">" >>$templates_index_path
    ;;
  *)
    input_type=$(jq -r '.["输入类型"]' <<<$search_value)
    field_name=$(jq -r '.["字段值"]' <<<$search_value)
    search_type=$(jq -r '.["搜索类型"]' <<<$search_value)

    case "$input_type" in
    'input')
      echo "                $key: <input id=\"${field_name}$search_type\" class=\"form-control input-sm inline w200\" max-length=\"20\" name=\"${field_name}$search_type\" type=\"text\" value=\"{{ request.GET.${field_name}$search_type }}\">" >>$templates_index_path
      ;;
    'select')
      echo "                $key: <select name=\"${field_name}$search_type\" class=\"form-control input-sm inline w80\">" >>$templates_index_path

      select_keys=($(jq -r '.["输入范围"]' <<<$search_value | jq -r 'keys_unsorted[]'))
      select_values=($(jq -r '.["输入范围"][]' <<<$search_value))

      for i in "${!select_keys[@]}"; do
        echo "                  <option value=\"${select_keys[i]}\" {% if request.GET.${field_name}$search_type == '${select_keys[i]}' or request.GET.${field_name}$search_type == '${select_keys[i]}'|add:'0' %}selected{% endif %}>${select_values[i]}</option>" >>$templates_index_path
      done

      echo "                </select>" >>$templates_index_path
      ;;
    *)
      echo -e "尚未兼容当前输入类型: $input_type"
      if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
      ;;
    esac
    ;;
  esac

  remainder=$(($index % 4))
  if [[ index -ne 0 && $remainder -eq 0 ]]; then
    echo -e "              </div>\n              <div class='mb10'>" >>$templates_index_path
  fi
done

cat "$main_path/formwork/templates/index/200.html" >>$templates_index_path

if [ "$need_new_edit_delete" == 'true' ]; then
  echo "                <a class=\"btn btn-sm btn-primary\" href=\"{% url '$templates_create_url' %}\">新增</a>" >>$templates_index_path
fi

cat "$main_path/formwork/templates/index/250.html" >>$templates_index_path



echo '写入表头'
for key in "${list_keys[@]}"; do
  echo "                  <th>$key</th>" >>$templates_index_path
done

cat "$main_path/formwork/templates/index/300.html" >>$templates_index_path

echo '写入列表字段'
for index_ in "${!list_keys[@]}"; do
  list_key=${list_keys[index_]}
  list_value=${list_values[index_]}
  search_value="$(jq -r --arg key $list_key '.["页面参数"]["列表"]["搜索字段"]["\($key)"]' <<<$params)"

  case "$search_value" in
  'null')
    echo "                    <td>{{ object.$list_value |default:\"\" |truncatewords:\"80\" }}</td>" >>$templates_index_path
    ;;
  *)
    input_type=$(jq -r '.["输入类型"]' <<<$search_value)
    field_name=$(jq -r '.["字段值"]' <<<$search_value)
    search_type=$(jq -r '.["搜索类型"]' <<<$search_value)

    case "$input_type" in
    'select')
      select_keys=($(jq -r '.["输入范围"]' <<<$search_value | jq -r 'keys_unsorted[]'))
      select_values=($(jq -r '.["输入范围"][]' <<<$search_value))

      echo "                    {% str_ object.$list_value as object_$list_value %}" >>$templates_index_path
      echo "                    <td>" >>$templates_index_path

      echo "                      {% if False %}" >>$templates_index_path
      for i in "${!select_keys[@]}"; do

        if [[ ${select_keys[i]} =~ .*\x.* ]]; then
          echo "                      {% elif object_$list_value == \"b'${select_keys[i]}'\" %} ${select_values[i]}" >>$templates_index_path
        else
          echo "                      {% elif object_$list_value == \"${select_keys[i]}\" %} ${select_values[i]}" >>$templates_index_path
        fi
      done
      echo "                      {% else %} {{ object.$list_value }}" >>$templates_index_path
      echo "                      {% endif %}" >>$templates_index_path

      # echo "                      {% if object_processed == "b'\x00'" %}未处理{% endif %}" >>$templates_index_path
      echo "                    </td>" >>$templates_index_path
      ;;
    *)
      echo -e "尚未兼容当前输入类型: $input_type"
      if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
      ;;
    esac
    ;;
  esac
done

cat "$main_path/formwork/templates/index/400.html" >>$templates_index_path

if [ "$need_new_edit_delete" == 'true' ]; then
  echo "                          <li><a href=\"{% url '$templates_update_url' object.id %}\">修改</a></li>" >>$templates_index_path
  echo "                          <li>
                            <form action=\"{% url '$templates_delete_url' object.id %}\" method=\"post\">
                              {% csrf_token %}
                              <input type=\"submit\" value=\"删除\" class='btn btn-link delete_btn' onclick=\"return confirm('确定要删除吗?')\"/>
                            </form>
                          </li>" >>$templates_index_path
fi

cat "$main_path/formwork/templates/index/500.html" >>$templates_index_path

cat $templates_index_path | sed "s/驼峰类名/$className/g" | sed "s/蛇形类名复数间隔/$class_names_split/g" | sed "s/蛇形类名复数/$class_names/g" | sed "s/蛇形类名/$class_name/g" | sed "s/管理名称/$manage_name/g" | sed "s/子项目名称/$sub_project_name/g" >$templates_index_path

# TODO: 有时流程走完后文件内容为空 猜测与多个循环产生的子进程有关
if [ ! -s "$templates_index_path" ];then
  echo "文件为空: $templates_index_path"
  if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then return 255; else exit 255; fi
else
  cat $templates_index_path
fi