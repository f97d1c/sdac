#!/bin/bash --login

# 项目根目录下
main_path="$(cd `dirname $0`;pwd)/../"

function main (){
  source "$main_path/shell/formwork/初始化参数.sh" $1
  if [[ ! $? -eq 0 ]];then return 255;fi

  source "$main_path/shell/formwork/模型代码.sh"
  if [[ ! $? -eq 0 ]];then return 255;fi

  source "$main_path/shell/formwork/视图代码.sh"
  if [[ ! $? -eq 0 ]];then return 255;fi

  source "$main_path/shell/formwork/页面-列表代码.sh"
  if [[ ! $? -eq 0 ]];then return 255;fi

  source "$main_path/shell/formwork/页面-详情代码.sh"
  if [[ ! $? -eq 0 ]];then return 255;fi

  source "$main_path/shell/formwork/页面-表单代码.sh"
  if [[ ! $? -eq 0 ]];then return 255;fi

  source "$main_path/shell/formwork/路由代码.sh"
  if [[ ! $? -eq 0 ]];then return 255;fi

  return 0
}

loop_count=0

function exec_main () {
  code=1
  main "$1"
  code=$?
  while [[ $code -ne 0 ]] && [[ $loop_count -lt 300 ]]
  do
    loop_count=$((loop_count+=1))
    exec_main "$1"
  done
}

if [ "$1" == 'ALL' ]; then
  find $main_path/config/pages/*.json -type f -print0 | while IFS= read -r -d $'\0' file; do 
    file_name=${file##*/}
    prue_name=${file_name%.*}
    echo "开始处理: $prue_name"
    exec_main "$file"
  done
elif [ "$1" == 'LAST' ]; then
  last_file=$(ls -t $main_path/config/pages/* | head -1)
  prue_name=${last_file%.*}
  echo "开始处理: $prue_name"
  exec_main "$last_file"
else
  exec_main "$1"
fi

# bash shell/脚手架-列表及详情.sh ALL
# bash shell/脚手架-列表及详情.sh LAST
# bash shell/脚手架-列表及详情.sh "$PWD/config/pages/公告正文.json"